#PERL Proxy List Validator
----------------------------

> A simple multi-threaded command line utility to test a list of proxy IP addresses for accessibility against a specific URL, ie. google.com

###Requirements
------------

Requires Threading support within PERL.

Text file of proxy IP addresses.  Sample file contents:

	1.2.3.4:80
	1.3.4.5:8080
	1.4.3.4:80
	1.5.4.5:8080
	1.6.3.4:80
	1.7.4.5:8080


###How-To
------

1.  run via command-line:
	
		$ perl main.pl
		
2.  Enter proxy list filename when prompted: 

		What file to use? [filename.txt]

3.  Enter filename to save validated proxy addresses to:

		What file to save to? [savename.txt]

4.  Enter which URL to test against:

		Enter url to check against: [google.com]

5.	Enter number value for Timeout in seconds to use in connections

		Timeout in seconds:[10]

6.	**Running..**

7.	**Finished.**

8.  Validated list will be stored in file input in step 3.
